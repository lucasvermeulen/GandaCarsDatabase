FROM microsoft/mssql-server-linux:2017-latest
ENV ACCEPT_EULA=Y
ENV MSSQL_SA_PASSWORD=Lucas1234 
ENV MSSQL_TCP_PORT=1433

#script bij nieuwe server
ADD scriptBijNieuweServer.sh /scriptBijNieuweServer.sh
RUN chmod +x /scriptBijNieuweServer.sh
RUN /scriptBijNieuweServer.sh
RUN rm /scriptBijNieuweServer.sh

#CMD script
ADD cmdScript.sh /usr/local/bin/shell.sh
RUN chmod 777 /usr/local/bin/shell.sh
CMD /usr/local/bin/shell.sh; sleep infinity