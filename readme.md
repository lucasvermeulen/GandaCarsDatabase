# Loonberekening Ganda Cars

Dit is het loonberekeningsporject van GandaCars (Gent). Deze applicatie werd geschreven door Lucas Vermeulen in opdracht van GandaCars.

# Gitlab
Gitlab is zo opgezet dat bij het pushen naar de master, er automatisch wordt gebuild en er een docker image op docker hub wordt gezet. Via de dockercontainer 'Ouroboros' zal elke 180 seconden gekeken worden voor een nieuwe versie, zodat alle containers up-to-date blijven. Dit is het geval voor de GandaCarsAngular, GandaCarsAPI en de GandaCarsDatabase.

# Setup: Docker

Alle docker containers kunnen in 1 keer opgezet worden via docker-compose: 
> docker-compose down
> docker-compose build
> docker-compose up

Alles van docker verwijderen:
> docker system prune -a --volumes

In de docker-compose.yml file staan er ook bepaalde lijnen gecommentarieerd, om de applicaties lokaal te laten lopen. Zo kunnen bepaalde zaken getest worden.
Er worden 5 docker containers aangemaakt:

## Ouroboros

Houd alle docker containers up to date met de docker containers te vinden op dockerHub. https://github.com/pyouroboros/ouroboros 

## Portainer

Visualisatie voor docker containers
>**Bereikbaar via:** http://localhost:9000/

## db-server

Hier wordt een microsoft 'mssql-server-linux' server opgezet. Daarna worden er een aantal basis commando's ed. geïnstalleerd en de gitrepo GandaCarsDatabase gecloned (voor een backup). Ook wordt een crontab ingesteld dat er dagelijks een backup van de db plaats vindt. Deze backup wordt gepushed naar de branch backups op gitlab.
>**Bereikbaar via:** console via Portainer

## GandaCarsAPI

Zet een mcr.microsoft.com/dotnet/core/sdk:2.2 image op, en zorgt voor het opzetten van de C#/asp.net core rest API.

De rest API maakt gebruik van swagger!

>**Bereikbaar via:** http://localhost:7000/
>**Swagger:** http://localhost:7000/swagger


## GandaCarsAngular

Zorgt voor het draaien van de node.js Angular applicatie.
>**Berkeibaar via:** http://localhost/
>**Swagger:** http://localhost:7000/swagger


# Hosts
Er kan voor gekozen worden om op de lokale machine de host aan te passen. Dit zorgt voor een lokale verwijzing naar localhost of 127.0.0.1.

>**Toe te voegen:** 127.0.0.1 loonberekening

## Windows
In **C:\Windows\System32\drivers\etc\hosts**.

## Mac OS
In **Terminal > sudo nano /private/etc/hosts**

## Linux
In **Terminal > sudo vim /etc/hosts**